package com.company.filemanager;

import com.company.logger.Logger;
import com.company.usercontrol.Control;
import java.io.*;
import java.util.Locale;
import java.util.Scanner;


public class Filemanager
{
    Control control = new Control();
    File directorypath = new File("src/com/company/resources");
    Logger log = new Logger();


    public void listFiles()
    {
        //File array, retrieve files from directory.
        File[] filesList = directorypath.listFiles();

        //Print out names all the files.
        for(File file: filesList)
        {
            System.out.println("Filename: "+file.getName());
        }
        control.startUserInteraction();

    }

    public void listFilesByType()
    {

        File[] filesList = directorypath.listFiles();
        System.out.println("please type in filetype");

        //Get user input
        Scanner sc = new Scanner(System.in);

        //Store user input in lowercase
        String filetype = sc.nextLine().toLowerCase(Locale.ROOT);

        int counter = 0;
        for(File file:filesList)
        {

            if(file.getName().toLowerCase().endsWith(filetype))
            {
                System.out.println(file.getName());
                counter++;
            }
        }
        if(counter==0)
        {
            System.out.println("No "+filetype+" files");
        }
        control.startUserInteraction();
    }

    public void getName()
    {
        //Start timer
        double startTime = System.nanoTime();

        Control control = new Control();

        //Get txt file and print
        File file = new File("src/com/company/resources/Dracula.txt");
        System.out.println(file.getName());

        //End timer
        double endTime = System.nanoTime();

        //Convert timer to MS from NS
        double duration = (double)(endTime - startTime) / 1000000;

        //Call to WriteToFile() in Logger class.
        log.WriteToFile("User retrieved the name of the file, the duration of the operation took :" + duration + " MS\n");

        //Prompt user to continue operations
        control.fileInteraction();
    }

    public void getSize()
    {
        //Start timer
        double startTime = System.nanoTime();

        Control control = new Control();

        //Get txt file
        File file = new File("src/com/company/resources/Dracula.txt");

        //Store filesize in B
        double fileSizeInBytes = file.length();

        //Convert to KB
        double fileSizeInKB = fileSizeInBytes / 1024;

        //Convert to MB and print
        double fileSizeInMB = fileSizeInKB / 1024;
        System.out.println(fileSizeInMB+" MB\n"+fileSizeInKB+" KB\n");

        //End time
        double endTime = System.nanoTime();

        //Convert timer to MS from NS
        double duration = (double)(endTime - startTime) / 1000000;

        //Call to WriteToFile() in Logger class.
        log.WriteToFile("User retrieved the size of the file, the duration of the operation took: " + duration + " MS\n");
        control.fileInteraction();
    }

    public void getLines()
    {
        double startTime = System.nanoTime();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("src/com/company/resources/Dracula.txt"))) {
            //Start lines at 1 as the while loop checks if next line is not null
            int lines = 1;
            String line;
            while ((line = bufferedReader.readLine()) != null)
            {
               lines++;
            }
            System.out.println(lines);
        }
        catch (Exception ex) {
            System.out.println("Error has occurred");
        }
        double endTime = System.nanoTime();
        double duration = (double)(endTime - startTime) / 1000000;
        log.WriteToFile("User retrieved total lines of the file, duration of the operation took: " + duration + "MS\n");
        control.fileInteraction();

    }

    public void searchWord()
    {
        //Start timer
        double startTime = System.nanoTime();

        //Try-catch block in case file is not found, corruped etc.
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("src/com/company/resources/Dracula.txt"))) {
            String line;

            System.out.println("please type word to search");

            //Get user input
            Scanner sc = new Scanner(System.in);

            //Store user input in lowercase
            String input = sc.nextLine().toLowerCase(Locale.ROOT);

            int count = 0;

            //Iterate through the lines
            while((line=bufferedReader.readLine()) != null)
            {
                //Replace all non characters from a-z(also uppercase) and ' with space, converts to lowercase and splits the sentence in to words. Store these words in a String array.
                String[] words = line.replaceAll("[^a-zA-Z']", " ").toLowerCase().split("\\s+");

                //Iterate through all the words in the array, checks if it matches with input
                for(String word : words)
                {
                    if(word.equals(input))
                    {
                        count++;
                    }
                }

            }
            System.out.println(count);

            //End timer
            double endTime = System.nanoTime();

            //Convert from NS to MS
            double duration = (double)(endTime - startTime) / 1000000;

            //Call to WriteToFile method in Logger class
            log.WriteToFile("User searched for the term '" + input + "' in the file, duration of the operation took: " + duration + "MS\n");
            control.fileInteraction();

        }
        catch (Exception ex) {
            System.out.println("An error has occurred");
        }
    }

}
