package com.company;

import com.company.logger.Logger;
import com.company.usercontrol.Control;

public class Main {

    public static void main(String[] args)
    {
        Control control = new Control();
        control.startUserInteraction();
    }
}
