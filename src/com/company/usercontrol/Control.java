package com.company.usercontrol;

import com.company.filemanager.Filemanager;
import java.util.Scanner;

public class Control
{
    Filemanager filemanager;

    public void startUserInteraction()
    {
        filemanager = new Filemanager();
        System.out.println("\nWelcome to the file manager");
        System.out.println("Type 1 to list all the files in the resources directory \nType 2 to list all the files of a specific type\nType 3 get in depth information about the txt file\nType q to exit\n");

        //Get user input
        Scanner sc = new Scanner(System.in);

        //Store user input in option variable
        String option = sc.nextLine();

        //If user picked a valid option, goes into switch cases. Otherwise start method again to prompt for new input.
        if(option.equals("1") || option.equals("2") || option.equals("3") || option.equals("q"))
        switch (option) {
            case "1" -> filemanager.listFiles();
            case "2" -> filemanager.listFilesByType();
            case "3" -> fileInteraction();
            case "q" -> System.exit(0);
        }
        else
        {
            startUserInteraction();
        }
    }

    public void fileInteraction()
    {
        filemanager = new Filemanager();
        System.out.println("Type 1 to list name of the text file\nType 2 to get size\nType 3 to get total lines\nType 4 to search if a specific word exists in the file\nType 5 to go back to main menu");

        //Get user input
        Scanner sc = new Scanner(System.in);

        //Store user input in option variable
        String option = sc.nextLine();

        //If user picked a valid option, goes into switch cases. Otherwise start method again to prompt for new input.
        if(option.equals("1") || option.equals("2") || option.equals("3") || option.equals("4") || option.equals("5"))
        {
            switch (option) {
                case "1" -> filemanager.getName();
                case "2" -> filemanager.getSize();
                case "3" -> filemanager.getLines();
                case "4" -> filemanager.searchWord();
                case "5" -> startUserInteraction();
            }
        }
        else
        {
            System.out.println("Please type in a valid option\n");
            fileInteraction();
        }

    }
}
