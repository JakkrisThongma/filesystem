package com.company.logger;

import java.io.FileWriter;
import java.io.IOException;

public class Logger
{

    public void WriteToFile(String input)
    {
        //Try-catch in case an error occurs
        try
        {
            //Create a FileWriter object. This also checks if the file exists, if the file does not exist a new one will created at the path given. Append set to true to not overwrite.
            FileWriter writer = new FileWriter("src/com/company/resources/log.txt", true);
            System.out.println(input);
            writer.write(input);
            //Close the stream to the file.
            writer.close();
        }
        catch(IOException e)
        {
            System.out.println("Error has occurred");
        }
    }
}
